# Changelog

## [unreleased]

## [0.2]

### Added

- CHANGELOG.md
- test case for the batch_params functionality

### Fixed

- use of deprecated pandas api in batch_params

### Changed

- move temporary storage from parallel hdf5 to simplified tile storage
  removing the dependency on h5py (and the parallel-io feature)
- interpret `noise-corr` parameter as boolean. This removes the capability
  to have more than 1 noise-corr iteration but can result in significant
  speedup.

## [0.1]

initial release
