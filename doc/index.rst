.. gapstop documentation master file, created by
   sphinx-quickstart on Fri Nov 10 11:07:02 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GAPSTOP's documentation!
===================================

**GAPSTOP**:sup:`TM` is GPU-Accelerated Python STOPgap for
Template Matching.

**GAPSTOP** is able to leverage the power of GPU accelerated multi-node HPC
systems to be efficiently used for template matching. It speeds up template
matching by using an MPI-parallel layout and offloading the compute-heavy
correlation kernel to one or more accelerator devices per MPI-process
using `jax <https://github.com/google/jax>`_.

Installation
------------

GAPSTOP can be installled with pip:

.. code-block:: Bash

   pip install "gapstop @ git+https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm.git"

Dependencies
^^^^^^^^^^^^

Beside some python-only dependencies that can be handled automatically by pip,
gapstop explicitly depends on:

* MPI. In case a working MPI implementation is available during installation,
  ``mpi4py`` will be installed automatically. It might be necessary to set the
  environment variable ``MPICC`` to point to the ``mpicc`` compiler wrapper.
  Please refer to the `official documentation
  <https://mpi4py.readthedocs.io/en/stable/install.html>`_ for details.

* GAPSTOP depends on ``jax`` and ``jaxlib``. A CPU-only version of gapstop can
  be readily installed by specifying the optional ``[cpu]`` dependency, e.g.:

  .. code-block:: Bash

     pip install "gapstop[cpu] @ git+https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm.git"

  To use a GPU enabled version, please refer to the
  `jax documentation <https://jax.readthedocs.io/en/latest>`_ for
  information on the installation of jaxlib with GPU support on your system.

Usage
-----

See ``gapstop --help`` for a short help message.

For a more detailed introduction please have a look at the :ref:`usage`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage

* :ref:`genindex`
* :ref:`search`
