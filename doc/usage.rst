.. _usage:

User manual
===========

gapstop is controlled via the command line interface *gapstop*. See


  .. code-block:: Bash

     gapstop --help

Template Matching
-----------------

Template matching is started from *gapstop* via the *run_tm* subcommand:

  .. program-output:: gapstop run_tm --help

The number of tiles can be set explicitly via the *--ntiles* option. In that
case, it should ideally be an integral multiple of the number of
MPI ranks times the number of allocated GPUs per rank. This will avoid
recompilation of the GPU-kernel for left-over tiles. If unspecified, the number
of tiles will be set to the overall number of GPUs available to *gapstop*.

The mandatory *params* option is a parameter file controlling template
matching in `starformat`_, see :ref:`defaults`. Template matching can be
dispatched over several sets of parameters that are specified as multiple
lines in the *value*-section of the file, see also :ref:`batching`. *gapstop*
will process these sets of parameters sequentially.

.. _starformat: https://en.wikipedia.org/wiki/Self-defining_Text_Archive_and_Retrieval

.. _batching:

Parameter batching
------------------

Multiline input-files can be automatically generated for different tomograms via
the *gapstop batch_params* subcommand:
  
  .. program-output:: gapstop batch_params --help

The resulting output can subsequently be used as input to *gaptstop run_tm*.

.. _defaults:

Default Parameters
------------------

Template matching is controlled via a set of parameters that have to be provided
as (multiline-) starfile. Some of these parameters are mandatory, some have
default values. To get an empty parameter template with defaults filled it,
*gapstop config* can be used, see:

  .. program-output:: gapstop config --help

Angles Generation
-----------------

*gapstop* can also be used to create angles to scan-over on-the-fly similar
to the
`cryocat.geom <https://github.com/turonova/cryoCAT/blob/main/cryocat/geom.py#L293>`_
module, when instead of *_anglist_name*, *_angincr* (and optionally *_angiter*,
*_phi_angincr* and *_phi_angiter* are specified in the parameters. Note that
*_anglist_name* always takes precedence when specified! The angles can also
be created explicitly by running the subcommand **generate_angles**:

  .. program-output:: gapstop generate_angles --help

Additional Notes
----------------

MPI support
^^^^^^^^^^^

Due to jax's memory allocation policy, it is necessary to either
either reduce the memory preallocation done by jax (see
`here  <https://jax.readthedocs.io/en/latest/gpu_memory_allocation.html>`_)
or bind GPUs explicitly to ranks so that GPUs are used exclusively by an
MPI-rank. With slurm the latter can, e.g., be achieved with
``--gpu-bind=[verbose,]per_task:X`` to bind each rank to the number ``X``
of GPUs.

Tiling
^^^^^^

Different mechanism for tiling can be set with the ``tiling`` parameter. Valid
values are 'legacy' (replicating the original matlab source), 'legacy\_fix'
(fixing a bug in the legacy tiling) and 'new' (a new implementation that is
more balanced than the legacy version). By default, currently 'legacy\_fix'
is set but the idea is to swith to new in the near future.

.. toctree::
   :maxdepth: 2
