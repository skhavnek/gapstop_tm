import os
import json
import warnings

import numpy as np

from gapstop.config import read_params, read_wedgelist
from gapstop.prepare_template import get_template_and_mask
from gapstop.dd import compute_tiles
from gapstop.filter import generate_bpf
from gapstop.filter import generate_filters
from gapstop.filter._wedgemask import generate_wedgemask_slices
from gapstop.filter._exposure import generate_exposure
from gapstop.filter._ctf import generate_ctf


class RefData:
    """Class to handle and check against reference data for a specific test."""

    def __init__(self, refdir, regenerate):
        self.refdir      = refdir
        self.regenerate  = regenerate

        # (this is slightly brittle since only working for this flat test-file
        # hierarchy); TODO: improve in case
        if not refdir.exists(): #bootstrap only
            if not refdir.parent.exists():
                os.mkdir(refdir.parent)
            os.mkdir(refdir)
            with open(refdir / "meta.txt", "w") as fp:
                json.dump(dict(), fp)

    def _savetxt(self, name, arr):
        if np.issubdtype(arr.dtype, np.floating):
            np.savetxt(name, arr, fmt="%.18e")
        elif np.issubdtype(arr.dtype, np.integer):
            np.savetxt(name, arr, fmt="%d")
        else:
            raise TypeError(f"array's of type {arr.dtype} cannot be stored yet.")
        
    def _genref(self, arr, name):
        if isinstance(arr, list):
            arr = np.array(arr)
        with open(self.refdir / "meta.txt", "r") as fp:
            meta = json.load(fp)
        with open(self.refdir / "meta.txt", "w") as fp:
            meta[name] = arr.shape
            json.dump(meta, fp)
            
        self._savetxt(
            self.refdir / f"{name}.txt",
            arr.reshape((-1,arr.shape[-1]))
        )
        msg = f"new ref-data has been written to {self.refdir}/{name}.txt" \
               " rerun test without --regenerate-refvalues"
        warnings.warn(msg)

  
    def _getrefs(self, names):
        if not isinstance(names, list):
            names = [names]
        with open(self.refdir / "meta.txt", "r") as fp:
            meta = json.load(fp)
        refs = {}
        for n in names:
            refname = self.refdir / f"{n}.txt"
            refs[n] = np.loadtxt(refname).reshape(meta[n])
        return refs

    def check_refs(self, **kwargs):
        for name, val in kwargs.items():
            try:
                ref = self._getrefs(name)
                assert np.allclose(val, ref[name])
            except Exception as e:
                if self.regenerate:
                    self._genref(val, name)
                else:
                    raise e

def test_bpf(request, genref):
    """test bandpass filters."""
    refbase = request.path.parent / "ref" / request.path.stem
    refdir  = refbase / request.node.name
    refdata = RefData(refdir, genref)

    p = {"lp_rad": 2., "hp_rad": 1., "lp_sigma": 1., "hp_sigma": 1}
    bpf = generate_bpf([(6,6), (12,12)], p)

    assert len(bpf) == 2
    refdata.check_refs(bpftmpl=bpf[0], bpftile=bpf[1])

def test_wedgemask(request, genref):
    """test wedgemask creation."""

    # reference data
    refbase = request.path.parent / "ref" / request.path.stem
    refdir  = refbase / request.node.name
    refdata = RefData(refdir, genref)

    # slim mock data
    p = {"lp_rad": 2., "hp_rad": 1., "lp_sigma": 1., "hp_sigma": 1}
    bpf = generate_bpf([(6,6,6), (12,12,6)], p)
    wl = {"tilt_angle": [0,20,70]}
    idx, weight, bin, tile_bin = generate_wedgemask_slices(wl, *bpf)

    # prepare idx to be checked against refvalues
    # (idx is list of 3-tuples of arrays with equal length within a tuple but 
    #  different length accross tuples; and refvalues simply stores arrays)
    idx_d = {f"idx-{i}": np.array(v) for i,v in enumerate(idx)}

    refdata.check_refs(**idx_d, weight=weight, bin=bin, tile_bin=tile_bin)
    
def test_exposure(request, genref):
    """test exposure filter."""
    # reference data
    refbase = request.path.parent / "ref" / request.path.stem
    refdir  = refbase / request.node.name
    refdata = RefData(refdir, genref)

    # slim mock data
    p = {
        "binning": 1.0,
    }
    wl = {
        "tilt_angle": [0,20,70],
        "exposure": [0.0, 10.0, 12.3],
        "pixelsize": 1.0,
    }

    idx = []
    weight = np.zeros((6,6,6), dtype=np.float32)
    rng = np.random.default_rng(42)
    for i in range(3):
       i_idx   = rng.choice([0,1], weight.shape) 
       weight += i_idx
       idx.append(np.nonzero(i_idx))
    w_idx = np.nonzero(weight)
    weight[w_idx] = 1. / weight[w_idx]
    
    exp_filt = generate_exposure(wl, idx, weight, p)

    refdata.check_refs(exp_filt=exp_filt)

def test_ctf(request, genref):
    """test ctf filter."""
    # reference data
    refbase = request.path.parent / "ref" / request.path.stem
    refdir  = refbase / request.node.name
    refdata = RefData(refdir, genref)

    # slim mock data
    p = {
        "binning": 1.0,
    }
    wl = {
        "tomo_x": 6,
        "tomo_y": 6,
        "tomo_z": 6,
        "defocus": [3.5, 3.4, 3.7],
        "amp_contrast": 0.07,
        "cs": 2.7,
        "voltage": 300.0,
        "pixelsize": 1.0,
    }

    idx = []
    weight = np.zeros((6,6,6), dtype=np.float32)
    rng = np.random.default_rng(42)
    for i in range(3):
       i_idx   = rng.choice([0,1], weight.shape) 
       weight += i_idx
       idx.append(np.nonzero(i_idx))
    w_idx = np.nonzero(weight)
    weight[w_idx] = 1. / weight[w_idx]
    
    ctf_filt = generate_ctf(wl, idx, weight, p)

    refdata.check_refs(ctf_filt=ctf_filt)

def test_filters(request, test_data_mini, genref):
    """test entire filter pipeline with full mock data."""
    # test data
    td = test_data_mini

    # reference data
    refbase = request.path.parent / "ref" / request.path.stem
    refdir  = refbase / request.node.name
    refdata = RefData(refdir, genref)

    params      = td.path / "tm_param.star"
    n_tiles     = 5
    conf        = read_params(params).iloc[0]
    tmpl, mask  = get_template_and_mask(conf)
    conf["tiling"] = "legacy"
    c, tilesize = compute_tiles(conf, n_tiles, tmpl)

    wl = read_wedgelist(
        td.path / conf["wedgelist_name"]
    )

    tmplbpf, tilebpf   = generate_bpf([tmpl.shape, tilesize], conf)
    tmplfilt, tilefilt = generate_filters(tmplbpf, tilebpf, wl, conf)

    refdata.check_refs(tmplfilt=tmplfilt, tilefilt=tilefilt)
