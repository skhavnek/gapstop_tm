import os
import pathlib
import shutil
from collections import namedtuple

import numpy as np
import yaml
import emfile
import mrcfile

from gapstop.rotation import rotate_volume
from gapstop.config import read_angles

import pytest

rng = np.random.default_rng()

# -----------------------------------------------------------------------------
#                                                                   mock data -
# -----------------------------------------------------------------------------

def _spherical_mask(mask_size, radius, ndim = 3):
    if not mask_size % 2:
        raise ValueError("Use uneven mask size or center will be off.")
    center = mask_size // 2
    arr = np.linalg.norm(np.indices((mask_size,) * ndim) - center, axis=0)
    return arr <= radius

def _gen_tomo_tmpl_masks(wdir, specs, name):
    """Generate tomogram and mask for the testcase 'name' in folder 'root'."""
    volume_size = specs["volume_size"]
    part_size   = specs["part_size"]
    part_val    = specs["part_val"]
    part_coords = specs["part_coords"]
    part_angles = specs["part_angles"]
    dtype       = specs["dtype"]
    tomo_num    = specs["tomo_num"]

    angles_dict = {
        "anglist_name": wdir / "inputs" / "anglist.csv",
        "anglist_order": "zzx",
    }
    angles      = read_angles(angles_dict)

    if any(i % 2 == 0 for i in part_size):
        msg  = "For simplicity, only uneven particle sizes are valid."
        msg += f" test-case '{name}' has tmpl_size: {part_size}"
        raise ValueError(msg)

    # create template with some border
    tmpl_size = 2 * max(part_size) - 1
    tmpl      = np.ones(part_size, dtype=dtype) * part_val
    pad_size  = tuple([((tmpl_size-r) // 2, (tmpl_size-r) // 2) for r in part_size])
    tmpl      = np.pad(tmpl, pad_size)

    # create spherical mask
    mask = _spherical_mask(tmpl_size, tmpl_size // 2).astype(dtype)

    #create tomogram and tomo_mask
    volume    = np.zeros(volume_size, dtype=dtype)
    tomo_mask = np.zeros(volume_size, dtype=dtype)
    lr        = tmpl_size // 2
    for coord,euler in zip(part_coords, part_angles):
        print("euler:", euler)
        ind = (
            slice(coord[0]-lr,coord[0]+lr+1),
            slice(coord[1]-lr,coord[1]+lr+1),
            slice(coord[2]-lr,coord[2]+lr+1),
        )
        volume[ind]    = rotate_volume(tmpl, angles[euler])
        tomo_mask[ind] = 1.0

    # add random noise
#    volume += rng.random(volume.shape)

    # write all to input
    outdir = wdir / "inputs"
    emfile.write(outdir / "tmpl.em", data=tmpl)
    emfile.write(outdir / "mask.em", data=mask)
    mrcfile.write(outdir / f"tomo_{tomo_num}.rec", data=volume, voxel_size=1)
    mrcfile.write(outdir / "tomo_mask.mrc", data=tomo_mask, voxel_size=1)

def _interpolate_templates(wdir, specs, with_mask=False):
    """Substitute placeholders in templated input."""

    dz,dy,dx = specs["volume_size"]
    tomo_num = specs["tomo_num"]

    mask_flag = "_tomo_mask_name" if with_mask else "remove_line"
    mask_name = wdir / "inputs" / "tomo_mask.mrc" if with_mask else ""
  
    # replace dimensions in wegde_list
    with open(wdir / "inputs" / "wedge_list.star", "r") as fp:
        wedge_list = fp.read()
        wedge_list = wedge_list.format(x=dx, y=dy, z=dz, tnum=tomo_num)

    with open(wdir / "inputs" / "wedge_list.star", "w") as fp:
        fp.write(wedge_list)

    # replace flags and names in tm_params
    with open(wdir / "tm_param.star", "r") as fp:
        params = fp.read()
        params = params.format(
            rootdir        = str(wdir) + "/", #TODO: remove this with matlab sources
            tomo_num       = tomo_num,
            tomo           = wdir / "inputs" / f"tomo_{tomo_num}.rec",
            tomo_mask      = mask_flag,
            tomo_mask_name = mask_name,
        )
        params = params.replace("remove_line\n", "")
        params = params.replace("    ", "  ")

    with open(wdir / "tm_param.star", "w") as fp:
        fp.write(params)

def _test_data(request, tmp_path):
    """Generate test case input from templates in tests/data."""

    # prepare working directory
    source = request.path.parent / "data"
    wdir   = tmp_path / "wdir"
    shutil.copytree(source / "input_template", wdir)

    # read test case specs
    with open(source / "specs.yml", "r") as fp:
        specs = yaml.safe_load(fp)

    # generate data from templates
    name      = request.param.name
    with_mask = request.param.with_mask
    specs     = specs[name]
    _gen_tomo_tmpl_masks(wdir, specs, name)
    _interpolate_templates(wdir, specs, with_mask)

    class TestData:
        pass

    d = TestData()
    d.path  = wdir
    d.specs = specs

    return d

TestParam = namedtuple("TestParam", ["name", "with_mask"])
 
@pytest.fixture(
    params=[
        TestParam("rect1", False),
        TestParam("rect1", True),
        TestParam("rectrot", False),
    ],
)
def test_data(request, tmp_path):
    return _test_data(request, tmp_path)

@pytest.fixture(params=[TestParam("mini", False)])
def test_data_mini(request, tmp_path):
    return _test_data(request, tmp_path)

# -----------------------------------------------------------------------------
#              utilities to (re-)generate reference values in case of failure -
# -----------------------------------------------------------------------------
def pytest_addoption(parser):
    parser.addoption(
        "--generate-refvalues",
        action="store_true",
        help="generate reference values at test failure?",
    )

@pytest.fixture
def genref(request):
    return request.config.getoption("--generate-refvalues")
