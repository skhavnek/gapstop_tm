import subprocess
import os

import numpy as np
import emfile

import pytest

def test_gapstop(test_data):

    wdir  = test_data.path
    specs = test_data.specs
    params = wdir / "tm_param.star"
    
    cmd = ["gapstop", "run_tm", str(params)]

    r = subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=False
    )

    # skip check returncode due to some messages from OpenMPI and jax in stderr
    assert r.returncode == 0

    scores = emfile.read(wdir / "outputs" / "scores_0_10.em")[1]
    angles = emfile.read(wdir / "outputs" / "angles_0_10.em")[1]

    ref_coords  = np.array(specs["part_coords"])
    ref_angles  = np.array(specs["part_angles"])

    # get len(ref_coords) maxima, i.e., coordinates thereof
    max_coords = []
    for i in range(len(ref_coords)):
        mx = np.array(np.unravel_index(np.argmax(scores), scores.shape))
        scores[mx[0]-1:mx[0]+2, mx[1]-1:mx[1]+2, mx[2]-1:mx[2]+2] = 0.0
        max_coords.append(np.array(mx))
    max_coords = np.array(max_coords)

    # sort by first column
    ref_coords = ref_coords[np.argsort(ref_coords[:,0])]
    ref_angles = ref_angles[np.argsort(ref_coords[:,0])]
    max_coords = max_coords[np.argsort(max_coords[:,0])]
    max_angles = angles[max_coords[:,0], max_coords[:,1], max_coords[:,2]]

    for mx in max_coords:
        ang = angles[mx[0]-1:mx[0]+2, mx[1]-1:mx[1]+2, mx[2]-1:mx[2]+2]
        print(mx)
        print(ang)


    assert np.all(np.abs(ref_coords - max_coords) <= 1)
    assert np.all(np.abs(ref_angles - max_angles) == 0)
