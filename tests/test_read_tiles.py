import numpy as np
import mrcfile
import pytest

from gapstop.read_tm_tiles import read_tm_tiles

@pytest.fixture(scope="module")
def mrc_file(tmp_path_factory):
    fname = tmp_path_factory.mktemp("data") / "data.mrc"   
    data = np.arange(125, dtype=np.float32).reshape((5,5,5))
    mrcfile.write(fname, data)
    return fname

def test_read_tile(mrc_file):

    tilesize = np.array([3,3,2])
    c = {}
    c["es"] = np.array([[1,1,1]])
    c["ee"] = np.array([[4,4,3]])
    c["ts"] = np.array([[0,0,0]])
    c["te"] = np.array([[3,3,2]])

    tile = read_tm_tiles(mrc_file, c, tilesize, 0)
    result = np.arange(125, dtype=np.float32).reshape((5,5,5))[1:4,1:4,1:3]
    print(tile)
    print(result)
    assert tile.shape == result.shape 
    assert np.all(tile == result)
