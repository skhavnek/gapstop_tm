from itertools import product

import numpy as np
from scipy.spatial.transform import Rotation as R
from scipy.ndimage import map_coordinates

from gapstop.prepare_template import _symmetrize_volume, get_template_and_mask
from gapstop.config import read_params

import pytest

def test_symmetrize():
    """Test template symmetrization."""
    vol = np.zeros((40,40,40), dtype=np.float32)
    vol[8:-8,12:-12,10:-10] = 1

    # construct probe locations
    center = np.floor(np.array(vol.shape) / 2) #the 'center' as in stopgap
    probe = np.array([8, 12, 10])
    probe = probe - center

    sym = ['c', 'd']
    fold = ['1', '2', '3', '4', '5']
    syms = [i+j for i,j in product(sym, fold)] + ['i', 'o']

    for s in syms:
        svol = _symmetrize_volume(vol, s)

        # test for values at rotated probe
        pg     = R.create_group(s.upper())
        rprobe = (pg.apply(probe[[2,1,0]])+center)[:,[2,1,0]]
        vals   = map_coordinates(svol, rprobe.T, order=1)
        assert np.sum(vals) > 0.7

def test_prepare_template(test_data):
    """Test get_template_and_mask."""

    conf = read_params(test_data.path / "tm_param.star").iloc[0]
    tmpl, mask = get_template_and_mask(conf)

    ref_size = (2 * max(test_data.specs["part_size"]) - 1,) * 3
    assert tmpl.shape == ref_size
