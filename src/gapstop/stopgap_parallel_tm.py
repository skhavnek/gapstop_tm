"""Calculate flcf-scores.

This module provides the public API `stopgap_parallel_tm` which provides
the score calculation. This functionality is batched over a number of
tiles given by available accelerator devices.
"""
import pathlib
from functools import partial

import jax.numpy as np
from jax import vmap, pmap, lax
from jax._src.lax.parallel import pargmax
from jax.lax import pmax

from .read_tm_tiles import read_tm_tiles
from .rotation import rotate_volume

def _get_padsize(tile, padded_size):
    """Non-jittable calculation of per-tile padsize.

    Computing the padsize from the target tilesize and the size of the template
    cannot be done inside a jit since the padsize would be depending on a
    traced array.
    """
    pad    = np.array(padded_size) - np.array(tile.shape)
    after  = pad // 2
    before = after + ( pad % 2 )
    # the following looks a little overly complicated but like this it
    # works equally for jax.DeviceArray as well as numpy.ndarray
    return tuple([(i.item(), j.item()) for i,j in zip(before, after)])
    
def _normalize_under_mask(vol, mask):
    """Normalize ref[mask] to standard normal distribution."""

    m = np.nanmean(np.where(mask > 0, vol, np.nan))
    s = np.nanstd(np.where(mask > 0, vol, np.nan), ddof=1)
    vol = vol - m
    vol = vol / s
    return vol * mask;

def _autocorrelation(vol, mask=None):
    
    if not mask is None:
        vol = vol * mask

    # compute autocorrelation function
    ft   = np.fft.fftn(vol)
    corr = ft * np.conj(ft)
    acf  = np.real(np.fft.ifftn(corr))

    # shift and rescale
    acf = np.fft.ifftshift(acf) / acf[0,0,0]

    return acf

def _calculate_flcf(mref, mask, conjTarget, conjTarget2):
    """Calculate fast local correlation function.

    Original documentation:
    A function to calculate the "Roseman" style fast-local correlation
    function (doi:10.1016/S0304-3991(02)00333-9). The exact derivation I used
    comes from a combination of (doi:10.1016/S0304-3991(02)00333-9) and
    DYNAMO.

    Parameters
    ----------
    mref: array-like
        Masked and normalized reference; this has mean 0 and stdev
        1 for the pixels under the mask. 
    mask: array-like
        Mask applied to reference.
    conjTarget: array-like
        The complex conjugate of the Fourier transform of the target.
    conjTarget2: array-like
        The complex conjugate of the Fourier transform of the square of
        the target. 
    
    Returns
    -------
    out: jax.DeviceArray
        Map with correlation coefficients.
    """

    boxsize = np.array(mref.shape)
    n_pix = np.sum(mask)

    # Calculate initial Fourier transforms
    mref = np.fft.fftn(mref)
    mask = np.fft.fftn(mask)

    # Calculate numerator of equation
    numerator = np.real(np.fft.ifftn(mref * conjTarget))

    # Calculate denominator in three steps
    sigma_a = np.real(np.fft.ifftn(mask * conjTarget2) / n_pix)
    sigma_b = np.real(np.fft.ifftn(mask * conjTarget) / n_pix)**2
    denominator = n_pix * np.sqrt(sigma_a - sigma_b)

    # Shifted FLCL map
    cc_map = np.real(numerator / denominator)

    # Calculate map and do a much of flips to get orientation correct...
    cen    = np.floor(boxsize / 2).astype(int) + 1
    cc_map = np.flip(cc_map)
    cc_map = np.roll(cc_map, cen, (0,1,2))

    return cc_map

def _prepare_tmpl(euler, tmpl, rot_mask, tmpl_filt, padsize):
    """Rotate template and mask."""
    
    rot_tmpl = rotate_volume(tmpl, euler)
    rot_tmpl = np.real(np.fft.ifftn(np.fft.fftn(rot_tmpl) * tmpl_filt))
    rot_tmpl = np.pad(rot_tmpl, padsize)
    rot_tmpl = _normalize_under_mask(rot_tmpl, rot_mask)

    return rot_tmpl

def _prepare_mask(euler, mask, padsize):
    """Rotate template and mask."""
    
    rot_mask = rotate_volume(mask, euler)
    rot_mask = np.where(rot_mask < np.exp(-2.0), 0.0, rot_mask)
    rot_mask = np.pad(rot_mask, padsize)

    return rot_mask

def _calculate_scores(
    euler,
    tmpl,
    mask,
    padsize,
    conj_tile,
    conj_tile2,
    tmpl_filt,
    pr_tmpl = None,
    scoring_fcn = None,
):
    """Computing scores mapped over angles."""

    rot_mask = _prepare_mask(euler, mask, padsize)
    rot_tmpl = _prepare_tmpl(euler, tmpl, rot_mask, tmpl_filt, padsize)
    score    = _calculate_flcf(rot_tmpl, rot_mask, conj_tile, conj_tile2)

    if scoring_fcn == "scf":
        acf         = _autocorrelation(rot_tmpl, rot_mask)
        acf         = _normalize_under_mask(acf, rot_mask)
        conj_score  =  np.conj(np.fft.fftn(score))
        conj_score2 =  np.conj(np.fft.fftn(score**2))
        score       = _calculate_flcf(acf, rot_mask, conj_score, conj_score2)

    if not pr_tmpl is None:
        rra_tmpl = _prepare_tmpl(euler, pr_tmpl, rot_mask, tmpl_filt, padsize)
        noise    = _calculate_flcf(rra_tmpl, rot_mask, conj_tile, conj_tile2)
    else:
        noise = None

    return score, noise

def _prepare_tiles_tm(tile, tile_filt, apply_laplacian=False):
    """Prepare tiles tm without posf interface."""

    if apply_laplacian:
        tile = del2(tile)

    ft_tile = np.fft.fftn(tile)

    # Apply filter
    ft_tile   = ft_tile * tile_filt
    ft_tile   = ft_tile.at[0,0,0].set(0)
    filt_tile = np.real(np.fft.ifftn(ft_tile))

    conj_tile  = np.conj(ft_tile)
    conj_tile2 = np.conj(np.fft.fftn(filt_tile**2))

    return conj_tile, conj_tile2

@partial(
    pmap,
    in_axes=(0,) + (None,) * 10,
    static_broadcasted_argnums=[4,8,9],
)
def _stopgap_parallel_tm(
    tile,
    ang_list,
    tmpl,
    mask,
    padsize,
    tmpl_filt,
    tile_filt,
    pr_tmpl,
    scoring_fcn,
    apply_laplacian,
):
    """Score calculation batched over tiles."""

    conj_tile, conj_tile2 = _prepare_tiles_tm(
        tile,
        tile_filt,
        apply_laplacian,
    )

    def _agg(carry, euler):
        """scan-reduce scores/angles/noise with lax.scan."""
        # current values
        score, angles, noise, i = carry
        # new values
        _score, _noise = _calculate_scores(
            euler,
            tmpl,
            mask,
            padsize,
            conj_tile,
            conj_tile2,
            tmpl_filt,
            pr_tmpl,
            scoring_fcn,
        )
        # update
        _score = np.stack([score, _score])
        score = np.max(_score, 0)
        max_idx = lax.argmax(_score, 0, np.int32)
        angles  = np.where(max_idx == 1, i , angles)
        if not pr_tmpl is None:
            noise = np.maximum(noise, _noise)
        return (score, angles, noise, i + 1), None

    # init scan with (scores, angles, noise, idx)
    init   = (
        np.zeros_like(tile) - 2,
        np.zeros_like(tile, dtype=np.int32) - 1,
        None if pr_tmpl is None else np.zeros_like(tile) - 2,
        0,
    )
    (scores, angles, noise, _), _ = lax.scan(_agg, init, ang_list)

    # apply noise corrections
    scores_raw = scores
    if not pr_tmpl is None:
        scores = (scores - noise) / ( 1.0 - noise )
        scores = np.where(scores > 0, scores, 0)

    return scores, angles, noise, scores_raw

def stopgap_parallel_tm(
    params,
    angles,
    tmpl,
    mask,
    tmpl_filt,
    tile_filt,
    c,
    tilesize,
    pr_tmpl,
    tile_nums,
):
    """Dispatch score calculation over a batch of tiles.

    Parameters
    ----------
    params: dict-like
        stopgap parameters (legacy stopgap's 'p')
    angles:
        list of angles to scan over
    tmpl: array
        template
    mask: array
        template mask
    tmpl_filt: array
        template filter
    tile_filt: array
        tile filter
    c: dict-like
        tile extraction coordinates (legacy stopgap's 'o["c"]')
    tilesize:
        size of the tiles to compute
    pr_tmpl: array
        phase randomization
    tile_nums: list
        indices of tiles to batch compute per dispatch

    Returns
    -------
    score_map: jax.DeviceArray
        Array with (noise-corrected) correlation coefficients.
    angle_map: jax.DeviceArray
        Array with angles for maximal correlation coefficients.
    noise_map: jax.DeviceArray
        Array with maximal noise correction coefficients.
    raw_score_map: jax.DeviceArray
       Raw (uncorrected) correlation coefficients.
    """
    tiles = []
    for tidx in tile_nums:
      tiles.append(read_tm_tiles(params["tomo_name"], c, tilesize, tidx))
    tiles = np.stack(tiles)

    padsize = _get_padsize(tmpl, tilesize)

    score_map, angle_map, noise_map, scores_raw = _stopgap_parallel_tm(
        tiles,
        angles,
        tmpl,
        mask,
        padsize,
        tmpl_filt,
        tile_filt,
        pr_tmpl,
        params["scoring_fcn"],
        params["apply_laplacian"],
    )

    return score_map, angle_map, noise_map, scores_raw
