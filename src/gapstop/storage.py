"""Storage for gapstop."""
import os
import pathlib

import numpy as np

class TiledDataset:
    """Dataset that is stored as tiles in the filesystem.

    Such Dataset rely on tiles being processed by single processes
    only. So no guards on parallel access are implemented here!
    """

    def __init__(self, name):
        """Initialize dataset."""
        self.name = pathlib.Path(name)
        self.name.mkdir(exist_ok=True, parents=True)

    def __setitem__(self, i, tile):
        """Write tile i."""
        np.savez(self.name / f"{i:d}", tile)

    def __getitem__(self, i):
        """Get tile i."""
        tname = self.name / f"{i:d}.npz"
        if not tname.exists():
            raise IndexError(f"tile {i} not in the dataset.")
        return np.load(tname)["arr_0"]

    def size(self):
        return len([n for n in os.listdir(self.name) if n.endswith(".npz")])

    def remove(self):
        for f in os.listdir(self.name):
            os.remove(self.name / f)
        self.name.rmdir()

class TileStore:
    """Storage of tiled maps."""

    def __init__(self, root):
        """Initialize store under root."""
        self.name      = pathlib.Path(root)
        self.datasets  = {}

        self.name.mkdir(parents=True, exist_ok=True)

    def __getitem__(self, k):
        return self.datasets[k]
                  
    def create_dataset(self, name):
        """Append a dataset to the store."""
        if name in self.datasets:
            raise ValueError(f"Dataset {name} already exists in store.")
        td = TiledDataset(self.name / name)
        self.datasets[name] = td
        return td

    def size(self):
        if len(self.datasets) == 0:
            return 0
        l = [i.size() for i in self.datasets.values()]
        err = "Inconsistent number of tiles stored in datasets."
        assert len(set(l)) == 1, err
        return l[0]

    def remove(self):
        for d in self.datasets.values():
            d.remove()
        self.name.rmdir()
