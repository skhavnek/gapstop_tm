"""Provides tiling of the tomogram."""
import warnings

import numpy as np
import mrcfile
from sympy import factorint

from .util import read_volume, tomo_size_from_mrc

def _bounds_from_mask(mask):
    """Compute start/stop/width indices of cube defined by the mask."""
    idx = np.argwhere(mask > 0)
    if len(idx) == 0:
        msg = "Detected invalid mask during tiling: No masked region found. " \
              "Using full tomogram for tiling."
        warnings.warn(msg, category=RuntimeWarning)
        return np.vstack([(0,0,0), mask.shape, mask.shape])
    # stop-boundary is exclusive in python -> +1
    start, stop = np.min(idx, 0), np.max(idx, 0) + 1
    return np.vstack([start, stop, stop-start])

def _round_to_even(num):
    """round integers down to even."""
    return (num + num % 2).astype("int")

def _tilegrid(shape, n):
    """Get tilesize for tiling shape into n even tiles."""

    shape = np.array(shape)

    # sorted 3D tile-grid dimensions
    f = [1] if n < 2 else factorint(n, multiple=True) # strange factorint
    if len(f) % 3:                                    # for n < 2 !?
        f.extend([1] * ((3 - len(f) % 3) % 3))
    grid = np.sort(np.prod(np.reshape(f, (-1,3)), 0))

    # associate grid to shape 
    idx = np.argsort(shape)
    grid = grid[np.argsort(idx)]

    return grid

def _tilecoords_legacy(bounds, ntiles, padsize, limits, sort_fix=True):
    """Compute tile coordinates for an array with shape 'shape'.

    This routine computes several sets of indices used to do various 
    extractions/insertions of the tile-data from/into the tomogram
    or from/into data that lives on arrays that are padded wrt the
    plain non-overlapping tiles that are defined by the grid. In detail,
    these are indices of:
    * box (bs/be):        non-overlapping tiles within the tomogram
    * extraction (es/ee): padded tiles within the tomogram
    * tiles (ts/te):      padded data in tile-local coordinates
                          (at boundary tiles there is less data to insert!)
    * crop (cs/ce):       the box within a padded tile
    """
    msg = "The use of '_tilecoords_legacy' will be deprecated soon. " \
          "Please switch to tiling=new for tile computation."
    warnings.warn(msg, category=FutureWarning)

    # adjust to the fact that the following tile logic was
    # designed for 3d matlab arrays with (x,y,z) axes associated
    # that appear transposed compared to 3d matlab arrays with
    # with (z,y,x) axes.
    coord_swap = [2,1,0]
    bounds     = bounds[:,coord_swap]
    padsize    = padsize[coord_swap]
    limits     = limits[coord_swap]

    grid = _tilegrid(bounds[2], ntiles)

    # replay stopgap's inversion flaw to compute stopgap's tilesize in case
    # 'sort_fix == False'
    idx       = np.argsort(bounds[2])
    grid      = np.sort(grid)
    patchsize = _round_to_even(np.ceil(bounds[2,idx] / grid - 1))
    i_idx     = np.argsort(idx) if sort_fix else idx[idx]
    grid      = grid[i_idx]
    patchsize = patchsize[i_idx]
    tilesize  = patchsize + padsize

    n_tiles = np.prod(grid)

    # to match this ceil/linspace combo with the original implementation,
    # work in matlab index space, ie. start index + 1, stop index identical
    # since exclusive in python
    starts = [
        np.ceil(np.linspace(a,b,k)) for a,b,k in zip(bounds[0]+1, bounds[1], grid+1)
    ]
    diff = [np.diff(si) for si in starts]
    ends = [s[:-1] + d - 1 for s,d in zip(starts,diff)] 
    for e in ends:
        e[-1] += 1

    starts = np.stack(np.meshgrid(
        starts[0][:-1],
        starts[1][:-1],
        starts[2][:-1],
        indexing='ij',
    ))

    ends = np.stack(np.meshgrid(*ends, indexing='ij'))

    # box (non-overlapping)
    # transpose to get consistent with matlab
    bs = starts.reshape(3, n_tiles, order='F').T
    be = ends.reshape(3,n_tiles, order='F').T

    # extraction
    es = bs - np.floor(padsize/2)
    ee = es + tilesize - 1
    
    # tile-local insertion
    ts = np.ones((n_tiles,3), dtype=bs.dtype)
    te = np.tile(tilesize, (n_tiles,1))
   
    # crop extraction and pasting ...
    # ... start
    ds         = 1 - es
    ds_idx     = ds > 0
    ts[ds_idx] = ts[ds_idx] + ds[ds_idx]
    es[ds_idx]  = 1
    # ... stop
    de         = limits - ee
    de_idx     = de < 0
    ee[de_idx] = (de + ee)[de_idx]
    te[de_idx] = te[de_idx] + de[de_idx]

    # crop box
    cs = np.tile(np.floor(padsize/2) + 1, (n_tiles,1))
    ce = cs + (be - bs)

    # account for matlab-python indexing differences as well as
    # axis swap (see comment above)
    return {
        "bs": bs[:,coord_swap]-1, #matlab -> pyhon
        "be": be[:,coord_swap],
        "es": es[:,coord_swap]-1, #m->p
        "ee": ee[:,coord_swap],
        "ts": ts[:,coord_swap]-1, #m->p
        "te": te[:,coord_swap],
        "cs": cs[:,coord_swap]-1, #m->p
        "ce": ce[:,coord_swap],
    }, tilesize[coord_swap]
    
def _tilecoords(bounds, ntiles, padsize):
    """Compute tile coordinates for an array with shape 'shape'.

    This routine computes several sets of indices used to do various 
    extractions/insertions of the tile-data from/into the tomogram
    or from/into data that lives on arrays that are padded wrt the
    plain non-overlapping tiles that are defined by the grid. In detail,
    these are indices of:
    * box (bs/be):        non-overlapping tiles within the tomogram
    * extraction (es/ee): padded tiles within the tomogram
    * tiles (ts/te):      padded data in tile-local coordinates
                          (at boundary tiles there is less data to insert!)
    * crop (cs/ce):       the box within a padded tile
    """
    grid = _tilegrid(bounds[2], ntiles)

    patchsize = np.ceil((bounds[1]-bounds[0]) / grid).astype('int')
    tilesize  = patchsize + padsize
    pad_half  = np.floor(padsize/2).astype('int')
    n_tiles   = np.prod(grid)

    # grid of non-overlapping tile start indices
    # (the data for the last tile in each dimension might be smaller due to the
    #  ceiling but this is accounted for by inserting/extracting into/from
    #  the tile)
    starts = [
        np.ceil(np.linspace(a,b,k)) for a,b,k in zip(bounds[0], bounds[1], grid+1)
    ]
    starts = np.stack(np.meshgrid(
        starts[0][:-1],
        starts[1][:-1],
        starts[2][:-1],
        indexing='ij',
    )).astype('int')

    # box (non-overlapping)
    bs = starts.reshape(3, n_tiles).T
    be = np.clip(bs + patchsize, None, bounds[1])

    # extraction
    es = bs - pad_half
    ee = es + tilesize
    
    # tile-local insertion
    ts = np.zeros((n_tiles,3), dtype=bs.dtype)
    te = np.tile(tilesize, (n_tiles,1))
   
    # crop bounds for extraction and pasting ...
    # ... start
    ds_idx      = np.nonzero(es < 0)
    ts[ds_idx] -= es[ds_idx]
    es[ds_idx]  = 0
    # ... stop
    de          = bounds[1] - ee
    de_idx      = np.nonzero(de < 0)
    ee[de_idx]  = np.tile(bounds[1], (n_tiles,1))[de_idx]
    te[de_idx] += de[de_idx]

    # crop box
    cs = np.tile(pad_half, (n_tiles,1))
    ce = cs + (be - bs)

    return {
        "bs": bs,
        "be": be,
        "es": es,
        "ee": ee,
        "ts": ts,
        "te": te,
        "cs": cs,
        "ce": ce,
    }, tilesize
    
def compute_tiles(conf, ntiles, tmpl):
    """Generate indices to extract tiles from tomogram."""

    tomo_name      = conf["tomo_name"]
    tomo_mask_name = conf.get("tomo_mask_name")

    # get bounds for tiling from tomogram
    tomo_size = tomo_size_from_mrc(tomo_name)
    bounds    = np.array([[0,0,0], tomo_size, tomo_size])

    legacy_compat = "legacy" in conf["tiling"]

    # overwrite bounds from masked region in case
    if not tomo_mask_name is None:
      mask   = read_volume(tomo_mask_name)
      bounds = _bounds_from_mask(mask)
      if legacy_compat:
          bounds[2] = _round_to_even(bounds[2])

    scf = conf["scoring_fcn"]
    if scf == "flcf":
        padsize = np.array(tmpl.shape)
    elif scf == "scf":
        padsize = np.array(tmpl.shape) * 2
    else:
        raise ValueError("Invalid scoring function.")

    if legacy_compat:
        tile_idx, tilesize = _tilecoords_legacy(
            bounds,
            ntiles,
            padsize,
            tomo_size,
            "fix" in conf["tiling"],
        )
    elif conf["tiling"] == "new":
        tile_idx, tilesize = _tilecoords(bounds, ntiles, padsize)
    else:
        msg = "Unkown tiling mechanism; choose one of " \
              "['legacy', 'legacy_fix', 'new']"
        raise ValueError(msg)

    print(f"\n-- Domain decomposition:")
    print(f"method:            {conf['tiling']}")
    print(f"full tomogram:     {tomo_size}")
    print(f"decomposed region: {bounds[2]}")
    print(f"tilesize:          {tilesize}")

    return tile_idx, tilesize
    
class TileIdx:
    def __init__(self, idx):
        """Initialize with 'o["c"]'"""
        self.idx = {"crop": [], "box": [], "extract": [], "paste": []}
        for t in range(len(idx["cs"])):
            cs      = idx["cs"][t].astype("int")
            ce      = idx["ce"][t].astype("int")
            crop    = tuple([slice(cs[j], ce[j]) for j in range(3)])
            bs      = idx["bs"][t].astype("int")
            be      = idx["be"][t].astype("int")
            box     = tuple([slice(bs[j], be[j]) for j in range(3)])
            es      = idx["es"][t].astype("int")
            ee      = idx["ee"][t].astype("int")
            extract = tuple([slice(es[j], ee[j]) for j in range(3)])
            ps      = idx["ts"][t].astype("int")
            pe      = idx["te"][t].astype("int")
            paste   = tuple([slice(ps[j], pe[j]) for j in range(3)])
            self.idx["crop"].append(crop)
            self.idx["box"].append(box)
            self.idx["extract"].append(extract)
            self.idx["paste"].append(paste)

    def __getitem__(self, key):
        return self.idx[key]
