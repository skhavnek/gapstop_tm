"""Exposure filter."""
import warnings

import numpy as np

from .bandpass import _frequencyarray

def generate_exposure(wedgelist, slice_idx, slice_weight, params):
    """Generate exposure filter."""

    expo      = wedgelist["exposure"]
    a,b,c     = wedgelist.get("crit_exp_param", (0.245, -1.665, 2.81))
    pixelsize = wedgelist["pixelsize"] * params["binning"]

    freq_array = np.fft.ifftshift(
        _frequencyarray(slice_weight.shape, pixelsize)
    )

    exp_filt = np.zeros_like(slice_weight)
    for expi,idx in zip(expo, slice_idx):
        freqs          = freq_array[idx]
        exp_filt[idx] += np.exp( -expi / ( 2 * ( (a * freqs**b) + c ) ) )

    exp_filt *= slice_weight

    return exp_filt
