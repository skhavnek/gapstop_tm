"""CTF filter."""

import numpy as np
from jax.scipy.interpolate import RegularGridInterpolator
#from scipy.interpolate import RegularGridInterpolator

from .bandpass import _frequencyarray

def _ctf(defocus, pshift, famp, cs, evk, f):
    """Original 'docstring':
    %% sg_ctf
    % Calculate a CTF curve.
    %
    % WW 07-2018
    """

    defocus = defocus * 1.0e4
    cs      = cs * 1.0e7
    pshift  = pshift * np.pi / 180


    h     = 6.62606957e-34
    c     = 299792458
    erest = 511000
    v     = evk * 1000
    e     = 1.602e-19

    lam = (c * h) / np.sqrt(((2 * erest * v) + (v**2)) * (e**2)) * (10**10)
    w   = (1 - (famp**2))**0.5

    v   = ((np.pi * lam * (f**2)) * (defocus - 0.5 * (lam**2) * (f**2) * cs))
    v  += pshift
    ctf = (w * np.sin(v)) + (famp * np.cos(v))

    return ctf

def generate_ctf(wl, slice_idx, slice_weight, params):
    """Generate ctf filter."""

    tmpl_size = slice_weight.shape[0]
    full_size = max(wl["tomo_x"], wl["tomo_y"], wl["tomo_z"])
    pixelsize = wl["pixelsize"] * params["binning"]

    freqs_full = _frequencyarray((full_size,), pixelsize)
    freqs_crop = _frequencyarray((tmpl_size,), pixelsize)[tmpl_size//2:]

    f_idx = np.zeros(full_size, dtype='bool')
    f_idx[:tmpl_size] = 1
    f_idx = np.roll(f_idx, -tmpl_size//2)
    f_idx = np.nonzero(f_idx)[0]

    defocus = np.array(wl["defocus"])
    pshift  = wl.get("pshift", np.zeros_like(defocus))

    # microscope parameters
    famp = wl["amp_contrast"]
    cs   = wl["cs"]
    evk  = wl["voltage"]
    
    full_ctf = np.abs(_ctf(defocus[:,None], pshift[:,None], famp, cs, evk, freqs_full))
    ft_ctf   = np.fft.fft(full_ctf, axis=1)
    ft_ctf   = ft_ctf[:,f_idx] * tmpl_size / full_size
    crop_ctf = np.real(np.fft.ifft(ft_ctf, axis=1))
    crop_ctf = crop_ctf[:,tmpl_size//2:]

    ctf_filt = np.zeros_like(slice_weight)
    x        = np.fft.ifftshift(_frequencyarray(slice_weight.shape, pixelsize))
    for ictf, sidx in zip(crop_ctf, slice_idx):
        ip   = RegularGridInterpolator(
            (freqs_crop,),
            ictf,
            fill_value=0,
            bounds_error=False,
            method='linear',
        )
        ctf_filt[sidx] += ip(x[sidx])

    #? np.nan_to_num(ctf_filt)
    ctf_filt *= slice_weight

    return ctf_filt
